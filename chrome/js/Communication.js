(function(global, factory){
    factory(global?global:this);
})(window, function(win){
    function Message(act){
        var msg = act;
        if(typeof(act)=='string'){
            msg = (/^\w+$/gi.test(act))?{action:act}:{action:'all', data:act};
        }
        for(var k in msg){ this[k] = msg[k]; }
    };

    var Communication = function(heartbeat){
        var _me = this;
        chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
            message = new Message(message);
            var e = message.action?message.action:'all';
            // 注：接收到消息必须回应，即调用一下sendResponse, 否则会报错 The message port closed before a response was received.
            if (typeof sendResponse !== 'function'){ sendResponse = function(e){ return e;} }
            if(_me._events && _me._events[e]){
                sendResponse( _me._events[e].apply(_me, [message, sender]) );
            }else{
                sendResponse({errcode:1, data:'action is not defined !', argus:{message:message, sender:sender, sendResponse:sendResponse}});
            }
        });
    }
    Communication.prototype.onMessage=function(e, callback){
        e = e?e:'all';
        if(!this._events){this._events={};}
        if(this._events[e]){
            var preF = this._events[e];
            this._events[e] = function(){
                var data = {}, d1={};
                try{
                    data = preF.apply(this, arguments);
                    d1 = callback.apply(this, arguments);
                }catch(e){
                    return {errcode:2, data: e};
                }
                if(typeof(data)!='object'){ data={errcode:0, data:data}; }
                if(typeof(d1)!='object'){ d1={errcode:0, data:d1}; }
                for(var k in d1){ data[k] = d1[k]; }
                return data;
            };
        }else{
            this._events[e] = callback;
        }
    };

    /**
     * 向tab发送消息
     * @param  {[type]} tabid 默认为当前页
     * @param  {[type]} data  发送的数据
     * @return {[type]}       [description]
     */
    Communication.prototype.sendTabMsg=function(tabid, data, callback){
        var msg = new Message(data);
        callback = callback?callback:function(response){};
        if(!tabid){
            var _me = this;
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
                if(tabs.length<1)return ;
                tabid = tabs[0].id;
                _me.sendTabMsg(tabid, msg, callback);
            });
            return ;
        }
        if(typeof chrome.app.isInstalled!=='undefined'){
            try{
                chrome.tabs.sendMessage(tabid, msg, callback);
            }catch(e){}
        }
        return ;
    }
    Communication.prototype.sendMessage=function(data, callback){
        callback = callback?callback:function(){};
        var msg = new Message(data);
        if(typeof chrome.app.isInstalled!=='undefined'){
            chrome.runtime.sendMessage(msg, callback);
        }
    }

    win.Communication = win.communication = Communication;
});    