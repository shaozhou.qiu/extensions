
(function(global, factory){
factory(global?global:this);
})(window, function(win){
   'use strict'
   var EL = function(el){
      el = $(el);
      if(el.length<1)return [];
      var ret = [];
      $(el).each(function(){
         ret.push($.extend(this, new EL.fn()));
      });
      return ret;
   };
   EL.fn = function(){}
   EL.fn.prototype.getPosition=function(){

   }
   EL.fn.prototype.getRSize=function(){
      var wh = this.getSize();
      return {
         width : ($(this).css('padding-left').replace(/[^\d]+/, '')*1)
                  + wh.width
                  + ($(this).css('padding-right').replace(/[^\d]+/, '')*1)
         ,height : ($(this).css('padding-top').replace(/[^\d]+/, '')*1)
                  + wh.height
                  + ($(this).css('padding-bottom').replace(/[^\d]+/, '')*1)
      };
   }
   EL.fn.prototype.getSize=function(){
      return {
         width : $(this).width()
         ,height : $(this).height()
      };
   }

   win.NElement = EL;


});

