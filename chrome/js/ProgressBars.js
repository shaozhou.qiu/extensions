 
 (function(global, factory){
    factory(global?global:this);
})(window, function(win){
    'use strict'
	
    var ProgressBars = function(min, max){
        var _me = this, val=0, div = document.createElement("div")
        , pro = document.createElement("div");
        div.style.zIndex=document.all.length+9999;
        div.style.position = "fixed";
        div.style.top = div.style.left = "10px";

        div.style.width="300px";
        div.style.height="12px";
        div.style.borderRadius='5px';
        div.style.background='#fff';
        div.style.border="1px solid #d8d8d8";

        pro.style.height="100%";
        pro.style.background='red';
        this.min = min;
        this.max = max;
        Object.defineProperty(this, "value", {
            get:function(){ return val; }
            ,set:function(e){ 
                val=e*1; 
                var rate = _me.getRate(); 
                pro.style.width = (rate*100)+'%';;
            }
        });
        div.append(pro);
        this.view = function(){ return div; }
        // document.body.append(div)

    }
    ProgressBars.prototype.getRate=function(){
        return (this.value-this.min)/(this.max-this.min);
    }

    


    win.ProgressBars = ProgressBars;

});

