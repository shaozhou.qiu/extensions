 
 (function(global, factory){
    factory(global?global:this);
})(window, function(win){
    'use strict'
	var pageSearch=function(){
        var _me=this, asins = $("div[data-asin]");
        asins.each(function(){
            var asin = $(this).attr('data-asin');
            if(asin=='')return ;
            var view = _me.view(asin);
            _me.appendView(this, view);
        });

        var a = document.createElement('a');
        a.innerHTML = 'Locate asin from the results';
        a.onclick=function(){
            var asin = prompt('asin:','');
            if(!asin)return ;
            if(!/^[a-z\d]{10,}$/gi.test(asin))return ;
            _me.pageIdx=0;
            _me.locateAsin(asin, function(e){
                var url = _me.getUrl(e);
                if(e>0){
                    alert("asin on page "+e+" of results");
                    $(a).html("asin on page "+e+" of results");
                    a.onclick=function(){location.href = url; }
                }else{
                    $(a).html("No results found within 100 pages");
                }
            });
        };
        var el = $("#search").find("[data-component-type='s-result-info-bar']").find(".sg-col-14-of-20>div>div");
        (el).append(a);
    }
    pageSearch.prototype.getUrl=function(page){
        var m = /(.*\/\/[^\/]+\/s\?k=[^&]+).*/gi.exec(location.href);
        if(!m) return ;
        return page?m[1]+'&page='+ page:m[1];
    }
    pageSearch.prototype.locateAsin=function(asin, callback){
        var _me=this;
        this.pageIdx++;
        if(this.pageIdx>99){ callback(-1); return ;}
        var url = _me.getUrl(this.pageIdx);
        if(!url) return ;
        $.ajax({
            url:url 
            ,success:function(e){
                var ret = e.indexOf('data-asin="'+asin+'"');
                if( ret == -1 ){
                    _me.locateAsin(asin, callback);
                }else{
                    callback(_me.pageIdx);
                }
            }
        });
    }
    pageSearch.prototype.appendView=function(el, view){
        // $(el).find(">div>span>div>div>div").append(view);
        $(el).find(".a-spacing-medium").append(view);
    }
    pageSearch.prototype.view=function(asin){
        var div = document.createElement('div');
        div.innerHTML = 'Asin : ' + asin
        // amazon.getlisting(asin, function(e){
        //     console.log(e);
        // });
        return div;
    }

    win.amzPageSearch = pageSearch;

});

