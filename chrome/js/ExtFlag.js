(function(global, factory){
    factory(global?global:this);
})(window, function(win){
    var similar=function(s, t){
        var d = [s,t,s], ret=0, sum=0;
        for(var i=0; i<d.length; i++){
            if(!((i+1)<d.length)){break;}
            var d0 = d[i], d1 = d[i+1];
            for(var k in d0){
                sum ++; if(d0[k] == d1[k]) ret++;
            }
        }
        return (ret/sum).toFixed(3)*1;
    }
    , attr2json = function(el){
        var attr = {};
        for(var i=0; i<el.attributes.length; i++){a=el.attributes[i];attr[a.name]=a.value;}
        return attr;
    }
    , createEl = function(el, remark){
        el = NElement(el);
        if(el.length == 0)return null;
        var x = y = 0;
        if(el){
            t = $(el).offset().top;
            l = $(el).offset().left;
        }
        var div = document.createElement('div');
        $(div).css({"display": "inline"
            // 'top': t+'px', 'left': l+'px'
            // ,'margin-top':$(el).height()*-1+'px','margin-left':  $(el).width()*-1 + 'px'
            ,'height':'21px','width':'21px'
            ,'cursor':'help'
            ,'-webkit-text-stroke':'0.1px #fff'
            ,'font-size':'14px', 'color':'red'
            ,'position':'absolute'
        }).attr('title', remark).html("|◤");
        return div;
    }
    , getSelecter = function(el){
        var val = '';
        val += el.tagName;
        if(el.id) {val += "#"+el.id; return val;}
        else if(el.className) val += "."+el.className.split(' ')[0];
        // else val += ":nth-child("+ $(el).index() +")";
        var pel = $(el).parent();
        if(pel.length>0 && pel[0].tagName != "BODY"){
            var pslt = getSelecter(pel[0]);
            if(val.indexOf("#")==-1){
                var chds = $(pslt.replace(/:nth\-child\(/gi, ":eq(")+" > " + val);
                if(chds.length>1){ val += ":nth-child("+ chds.index(el) +")"; }
            }
            val = pslt + " > "+val;
        }else{
            val = "BODY > "+val;
        }
        return val;
    }

    ,Flag = function(){
        var _me = this;
        this.sessionKey = (location.host).replace(/[^\w]+/gi, '_')+'_flag';
        // this.clear();
        $(function(){ setTimeout(function(){_me.review();}, 500); });
    }
    Flag.prototype.review=function(){
        var _me = this;
        $(_me._hflag).remove();
        this.get(function(e){
            if(!(e instanceof Array)) return ;
            for(var i=0; i<e.length; i++)_me.show(e[i]);
        });
    }
    Flag.prototype.show=function(e){
        if(!e || !e.el)return;
        if(!this._hflag)this._hflag=[];
        var _me=this, el = new NElement(e.el), getSimilar=function(e, el){
            var sel = null, slm=0;
            for(var i=0; i<el.length; i++) {
                var sl = similar(e.attr, attr2json(el[i]));
                if(sl > slm){slm = sl; sel = i;}
            }
            return {slm:slm, el:el[sel]};
        };
        if(el.length == 0){
            setTimeout(function(){ _me.show(e); }, 2000);
            return ;
        }
        var ret = getSimilar(e, el), elregx=[
            [/\s*:\s*eq\s*\(\s*\d+\s*\)\s*/gi, ""]
            , [/.*?(([^>]+>?){3})$/gi, "$1"]
        ], n=0;
        while(ret.slm == 0 && n<elregx.length){
            var re = elregx[n++];
            el = new NElement(e.el.replace(re[0], re[1]));
            ret = getSimilar(e, el);
        }
        
        if(ret.slm > 0.6){
            var tel = ret.el;
            if(e.fp){
                if(e.fp != this.el2f(tel) ){
                    return ;
                }
            }
            var el = createEl(tel, e.remark);
            if(el==null)return ;
            _me._hflag.push(el);
            // $(e.el).before(el);
            // $(e.el).prepend(el);
            $(tel).after(el);
            $(el).click(function(){ _me.del(e); });
        }else{
            setTimeout(function(){ _me.show(e); }, 2000);
            return ;
        }
    }
    Flag.prototype.del=function(d){
        var _me = this;
        this.get(function(e){
            for(var k in e){
                if(e[k].el == d.el) {e.splice(k, 1);break;}
            }
            _me.set(e);
            _me.review();
        });
    }
    Flag.prototype.set=function(def){ 
        var data={};
        data[this.sessionKey] = def?def:[];
        chrome.storage.local.set(data);
    }
    Flag.prototype.clear=function(){this.set([]); this.review();}
    Flag.prototype.get=function(callback){
        var _me = this;
        chrome.storage.local.get(this.sessionKey, function(e){
            callback(e[_me.sessionKey]);
        });
    }
    Flag.prototype.fingerprint=function(txt){
        txt = txt.replace(/[^\da-zA-Z]+/gi, '');
        var l = txt.length, cv=0;
        if(l<1)return '';
        for(var i=0; i<l; i++){ cv += txt.substr(i,1).charCodeAt(); }
        cv = cv%255;
        if(l>1){
            return txt.substr(0,1).charCodeAt()+'-'+l+'-'+cv + txt.substr(l-1,1).charCodeAt();
        }
        return l+'-'+cv;
    }
    Flag.prototype.el2f=function(el){
        var txt = el.innerText.replace(/[^\da-zA-Z]+/gi, '');
        var reVal = this.fingerprint(txt);
        // for(var i=0 ; i<el.attributes.length; i++){ reVal+= this.fingerprint(el.attributes[i].value) ; }
        return reVal;
    }
    Flag.prototype.add=function(el, remark){
        if($(el).css("position")=="absolute" ||$(el).css("position")=="fixed" ){
            alert('can\'t add flag on fixed or absolute element');
        }
        var _me = this;
        var elslt = getSelecter(el);
        var attr = attr2json(el);
        elslt = (elslt+'').replace(/:nth\-child/gi, ":eq");
        el = $(elslt);
        if( el.length == 0 || el.length >1 ){
            alert('Does not exist or exists more than one!  #'+el.length);
            console.log(elslt);
            return ;
        }
        this.get(function(e){ 
            if(!e || !(e instanceof Array)) e=[];
            e.push({el:elslt, attr:attr, fp:_me.el2f(el[0]), remark:remark}); 
            _me.set(e); 
            _me.show(e[e.length-1]); 
        });
    };


    win.ExtFlag = new Flag();
});