((function(win){

    var MenuItem = function(title, callback, param){
        this.title = title;
        this.callback = callback;
        this.param = param;
        this.submenu = [];
    }
    MenuItem.prototype.add=function(){
        var m = new MenuItem(arguments[0], arguments[1]);
        this.submenu.push( m );
        return m;
    }
    MenuItem.prototype.remove=function(idx){
        if(!this.submenu)return this;
        this.submenu.splice(idx, 1);
        return this;
    }
    
    var contextMenus = function(title, callback, config){
        this.m = new MenuItem(title, callback, config);
    }
    contextMenus.prototype.add=function(){
        this.m.add.apply(this.m, arguments);
    }
    contextMenus.prototype.remove=function(){
        this.m.remove.apply(this.m, arguments);
    }
    contextMenus.prototype.show=function(){
        var m = arguments[0];
        if(this instanceof contextMenus) m = this.m;
        var conf = {
            "contexts": ["page", "selection", "image", "link"]
        };
        if(typeof(m.title)=='string'){
            conf.title = m.title;
        }else{
            for(var k in m.title)conf[k]=m.title[k];
        }
        conf.onclick = m.callback;
        if(this.id)conf.parentId = this.id;
        var id = chrome.contextMenus.create(conf);
        for(var i=0; i<m.submenu.length; i++){
            this.show.call({id:id}, m.submenu[i]);
        }
    }

    win.contextMenus=contextMenus;
})(window));