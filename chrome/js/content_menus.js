var content_menus = [
   {
      title : "ET"
      ,callback : null
      ,children : [
         {title :  "|◤ Clear Flag", callback : function(){ (new Communication()).sendTabMsg(null, {action:'onClearFlag', args:arguments}, function(response){}); }, children : []}
         ,{title : "|◤ Add Flag", callback : function(){ 
            (new Communication()).sendTabMsg(null, {action:'onAddFlag', args:arguments}, function(response){}); 
         }, children : []}

         ,{title : "♥ Web Color", callback : function(){ 
            (new Communication()).sendTabMsg(null, {action:'onSetWebColor', args:arguments}, function(response){}); 
         }, children : []}
      ]
   }
];