
(function(global, factory){
factory(global?global:this);
})(window, function(win){
   'use strict'
   var App = function(){
      for(var k in App.init){
         App.init[k].apply(this, []);
      }
   }
   ;
   
   // 初始化
   App.init = {
      // 初始化右键菜单
      initContextMenus : function(){
         if(typeof(contextMenus) == 'undefined')return ;
         if(typeof(content_menus) == 'undefined')return ;
         function cm(data, m){
            var menus = [];
            for(var i=0; i<data.length; i++){
               var d = data[i];
               var menu =m?m.add(d.title, d.callback):new contextMenus(d.title, d.callback);
               if(d.children && d.children.length>0){
                  cm(d.children, menu);
               }
               menus.push(menu);
            }
            return menus;
         }
         var menu = cm(content_menus);
         if(!menu || menu.length<1)return ;
         menu = menu[0];
         menu.show();
      }

   }

   var app = new App();


});

